
let dataBase = [];

import { currentTab } from "./app.js";

// html Elements
const taskContainer = document.getElementById(`todoTaskContainer`); 
const completedButton = document.getElementById("completed-btn");
const activeButton = document.getElementById("active-btn");
const allButton = document.getElementById("all-btn");
const deleteButton = document.getElementById("deleteall");
const inputBar = document.getElementById("input_bar");


export function updateDataBase(task, id) {

  let obj = {};

  obj["id"] = id;

  obj["task"] = task;

  obj["status"] = false;

  dataBase.push(obj);

  console.log(dataBase);

  if (currentTab === "All") {
    showAll();
  } else if (currentTab === "Active") {
    showActive();
  }
  checkStatus();
}

export function showActive() {

  const activeTasks = dataBase.filter((todo) => todo.status === false);

  taskContainer.innerHTML = "";

  activeTasks.forEach(todo => {

    taskContainer.innerHTML += `
     <div id = "todo${todo.id}" class="todo-task" >
     <div class = "todoTask-InnerDiv">
     <input class = "checkBox" id="${todo.id}" type="checkbox"> </div>  
     <label id = "task${todo.id}"><span>${todo.task}</span></label>
     </div>
 </div>`
  })

  checkStatus();
  completedButton.classList.remove("tab");
  activeButton.classList.add("tab");
  allButton.classList.remove("tab");
  deleteButton.style.display = "none";
  inputBar.style.display = "block";

}

export function showAll() {

  taskContainer.innerHTML = "";

  dataBase.forEach(todo => {

    if (todo.status === true) {
      taskContainer.innerHTML += `
      <div id = "todo${todo.id}" class="todo-task" >
      <div class = "todoTask-InnerDiv">
      <input class = "checkBox" id="${todo.id}" type="checkbox" checked  ></div>  
      <label id = "task${todo.id}"><span>${todo.task}</span></label>
      </div>
  </div>`

    } else {
      taskContainer.innerHTML += `
      <div id = "todo${todo.id}" class="todo-task" >
      <div class = "todoTask-InnerDiv">
      <input class = "checkBox" id="${todo.id}" type="checkbox"></div>  
      <label id = "task${todo.id}"><span>${todo.task}</span></label>
      </div>
  </div>`
    }
  })

  completedButton.classList.remove("tab");
  activeButton.classList.remove("tab");
  allButton.classList.add("tab");
  deleteButton.style.display = "none";
  inputBar.style.display = "block";
  checkStatus();
}


export function showCompleted(data) {

  taskContainer.innerHTML = "";

  if (data === undefined) {
    data = dataBase;
  }
  data.forEach(todo => {

    if (todo.status === true) {

      taskContainer.innerHTML += `
       <div id = "completedTasks" class="todoTaskCompleted" >
       <div class = "todoTask-InnerDiv">
       <input class = "checkBox" id="status${todo.id}" type="checkbox" checked  >
       <label id = "task${todo.id}"><span>${todo.task}</span></label>
       </div>
       <div id= "${todo.id}" class = "lowerDiv delbtn">
       <i  class="fa fa-trash" aria-hidden="true"></i>
       </div>
      
   </div>`

    }
  });
  deleteTodo();

  completedButton.classList.add("tab");
  activeButton.classList.remove("tab");
  allButton.classList.remove("tab");
  deleteButton.style.display = "block";
  inputBar.style.display = "none";
}

function checkStatus() {

  let allCheckboxs = document.querySelectorAll("input[type='checkbox']");

  allCheckboxs.forEach((box) => {
    box.addEventListener("click", () => {
      dataBase.forEach((dB) => {
        if (box.id == dB.id) {
          dB.status = true;
        }
      })
    })
  })
}

function deleteTodo() {

  let deleteBtn = document.querySelectorAll(".delbtn");
  deleteBtn.forEach((item) => {
    item.addEventListener("click", () => {
      dataBase.forEach((toItem, index) => {
        if (item.id == toItem.id) {
          dataBase.splice(index, 1);
          showCompleted();
        }
      })
    })
  })
}

export function deleteAll() {

  for (let index = dataBase.length - 1; index >= 0; index--) {

    if (dataBase[index]["status"] === true) {

      dataBase.splice(index, 1);
    };
  }

  showCompleted();
}


